#Machine Learning Assignment 1
#@Author Victor Gallen 38103

import numpy as np
import pandas as pd
from tkinter import *
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt

df = pd.read_csv("Data.csv", sep=";" , encoding="UTF-16", decimal =",")
#print(df)
#print(df.shape)

#Replacing M with 1 and F with 0, so we have float values instead of string
df.Gender.replace(['M', 'F'],[1,0], inplace=True)

#Converting the rest of the columns to float64 (Age and Distance)
df.apply(pd.to_numeric, errors='ignore')

#Same as above, Car = 1, No Car = 0
df.Mobility.replace(['Car', 'No car'], [1, 0], inplace=True)

#String to int for "Reason"
df.Reason = pd.Categorical(df.Reason)
df['Reason'] = df.Reason.cat.codes

#X are the input values, Y is the output
dataset = df.values
X = dataset[:,0:5]
Y = dataset[:,5]

def create_model(optimizer='rmsprop', init='glorot_uniform'):
    model = Sequential()
    model.add(Dense(12, input_dim=5, kernel_initializer=init, activation='relu'))
    model.add(Dense(8, kernel_initializer=init, activation='relu'))
    model.add(Dense(1, kernel_initializer=init, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    #Fitting the model
    return model

model = create_model('rmsprop', 'glorot_uniform')
prevmodel = model.fit(X, Y, epochs = 100, batch_size=10, validation_split=0.2, verbose=0)

print(prevmodel.history.keys())

#Assigning the corresponding names for the plot
plt.plot(prevmodel.history['acc'])
plt.plot(prevmodel.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()



model = KerasClassifier(build_fn=create_model, verbose=0)
optimizers = ['rmsprop', 'adam']
init = ['glorot_uniform', 'uniform', 'normal']
epochs = [50, 100, 150]
batches = [5, 10, 20]

param_grid = dict(optimizer=optimizers, epochs=epochs, batch_size=batches, init=init)

grid = GridSearchCV(estimator=model, param_grid=param_grid)
grid_result = grid.fit(X, Y)

print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
