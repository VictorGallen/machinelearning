#@Author
#Victor Gallen
#38103
#14.2.2018

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn.metrics import confusion_matrix
from sklearn.tree import export_graphviz
import matplotlib.pyplot as plt
import pydotplus
import io

#reading our file
df = pd.read_excel("DataProject2.xlsx")

#Setting each individual column to numerical values instead of strings
df.gender = pd.Categorical(df.gender)
df["gender"] = df.gender.cat.codes

df.SeniorCitizen = pd.Categorical(df.SeniorCitizen)
df["SeniorCitizen"] = df.SeniorCitizen.cat.codes

df.Partner = pd.Categorical(df.Partner)
df["Partner"] = df.Partner.cat.codes

df.Dependents = pd.Categorical(df.Dependents)
df["Dependents"] = df.Dependents.cat.codes

df.tenure = pd.Categorical(df.tenure)
df["tenure"] = df.tenure.cat.codes

df.PhoneService = pd.Categorical(df.PhoneService)
df["PhoneService"] = df.PhoneService.cat.codes

df.MultipleLines = pd.Categorical(df.MultipleLines)
df["MultipleLines"] = df.MultipleLines.cat.codes

df.InternetService = pd.Categorical(df.InternetService)
df["InternetService"] = df.InternetService.cat.codes

df.OnlineSecurity = pd.Categorical(df.OnlineSecurity)
df["OnlineSecurity"] = df.OnlineSecurity.cat.codes

df.OnlineBackup = pd.Categorical(df.OnlineBackup)
df["OnlineBackup"] = df.OnlineBackup.cat.codes

df.DeviceProtection = pd.Categorical(df.DeviceProtection)
df["DeviceProtection"] = df.DeviceProtection.cat.codes

df.TechSupport = pd.Categorical(df.TechSupport)
df["TechSupport"] = df.TechSupport.cat.codes

df.StreamingTV = pd.Categorical(df.StreamingTV)
df["StreamingTV"] = df.StreamingTV.cat.codes

df.StreamingMovies = pd.Categorical(df.StreamingMovies)
df["StreamingMovies"] = df.StreamingMovies.cat.codes

df.Contract = pd.Categorical(df.Contract)
df["Contract"] = df.Contract.cat.codes

df.PaperlessBilling = pd.Categorical(df.PaperlessBilling)
df["PaperlessBilling"] = df.PaperlessBilling.cat.codes

df.PaymentMethod = pd.Categorical(df.PaymentMethod)
df["PaymentMethod"] = df.PaymentMethod.cat.codes

df.TotalCharges = pd.Categorical(df.TotalCharges)
df["TotalCharges"] = df.TotalCharges.cat.codes

df.MonthlyCharges = pd.Categorical(df.MonthlyCharges)
df["MonthlyCharges"] = df.MonthlyCharges.cat.codes

df.Churn = pd.Categorical(df.Churn)
df["Churn"] = df.Churn.cat.codes

df.apply(pd.to_numeric, errors = "ignore")

df = df[["gender", "SeniorCitizen", "Partner", "Dependents","tenure", "PhoneService", 
"MultipleLines", "InternetService", "OnlineSecurity", "OnlineBackup", "DeviceProtection", "TechSupport", "StreamingTV", "StreamingMovies",
"Contract", "PaperlessBilling", "PaymentMethod","MonthlyCharges","TotalCharges","Churn"]]

df.apply(pd.to_numeric, errors='ignore')
#print(df.PaymentMethod.head())
#Replacing all empty strings with NAN, so we can drop them later
df.replace(" ",np.nan ,inplace=True)
#dropping NAN values (previously empty strings)
df = df.dropna()
#We drop the table column Churn from the dataset X
X = df.drop("Churn",axis=1)
#Y is only the column Churn, since this is the column we are trying to predict the outcome of
Y = df["Churn"]

X_train, X_test, Y_train, Y_test = train_test_split(X,Y,test_size=0.5, random_state=1)


#print(pd.DataFrame(confusion_matrix(Y_test,Y2_predict),columns=["Predicted not left","Predicted left"], index=["True not left", "True left"]))

#Model1
model1 = DecisionTreeClassifier(min_samples_split=100)
model1.fit(X_train, Y_train)
Y_predict = model1.predict(X_test)
print("The accuracy for model 1:", accuracy_score(Y_test, Y_predict)*100,"%")


#Model2
model2 = DecisionTreeClassifier(criterion="entropy",random_state=100,max_depth=100,min_samples_leaf=50,min_samples_split=100)
model2.fit(X_train,Y_train)
Y2_predict = model2.predict(X_test)
print("The accuracy for model 2:", accuracy_score(Y_test, Y2_predict)*100,"%")
#cool confusion_matrix just for testoutput before picture
#print(pd.DataFrame(confusion_matrix(Y_test,Y_predict),columns=["Predicted not left","Predicted left"], index=["True not left", "True left"]))
#print(pd.DataFrame(confusion_matrix(Y_test,Y2_predict),columns=["Predicted not left","Predicted left"], index=["True not left", "True left"]))



#Assigning the names for each column separately for the show_tree method
names = ["gender", "SeniorCitizen", "Partner", "Dependents","tenure", "PhoneService", 
"MultipleLines", "InternetService", "OnlineSecurity", "OnlineBackup", "DeviceProtection", "TechSupport", "StreamingTV", "StreamingMovies",
"Contract", "PaperlessBilling", "PaymentMethod","MonthlyCharges","TotalCharges"]    

#our method that prints out a .png file of our decision tree
#We choose to print out model1
def show_tree(tree, df, path):
    f = io.StringIO()
    export_graphviz(tree,out_file=f, feature_names=names,class_names=["Still a customer","Left"],filled = True, rounded = True)
    pydotplus.graph_from_dot_data(f.getvalue()).write_png(path)

show_tree(model1, df, "model2.png")