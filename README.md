# README #

### What is this repository for? ###
This repo consists of my work in the Machine Learning Masters Course I am attending currently. (Feb-March 2018)

### How do I get set up? ###

The projects in this course are done in python, multiple libraries have to be installed to run the code properly.
Mostly using scikit-learn, numpy, pandas, tensorflow, matplotlib etc for machine learning purposes.
Requirements.txt file coming up shortly, so you can easily install all requirements.

### Who do I talk to? ###

Contact me @ gallen.victor@gmail.com if you have any questions!
Happy coding :)